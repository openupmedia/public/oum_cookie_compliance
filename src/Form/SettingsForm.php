<?php

/**
 * @file
 * Contains Drupal\oum_cookie_compliance\Form\SettingsForm.
 */

namespace Drupal\oum_cookie_compliance\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\oum_cookie_compliance\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'oum_cookie_compliance.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oum_cookie_compliance_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('oum_cookie_compliance.settings');

    $form['oum_cookie_compliance_styling_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Styling settings'),
      '#open' => TRUE,
    ];

    $form['oum_cookie_compliance_styling_settings']['load_module_twig'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable default twig template provided by this module'),
      '#default_value' => $config->get('load_module_twig'),
      '#description' => t('Loads a twig template file for the default OUM layout & styling (included in the module).<br /><strong>Note:</strong> chaning this value requires a cache clear in order to work!'),
    ];

    $form['oum_cookie_compliance_styling_settings']['load_module_css'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable default CSS provided by this module'),
      '#default_value' => $config->get('load_module_css'),
      '#description' => t('Loads a CSS file for the default OUM layout & styling (included in the module).'),
    ];

    $form['oum_cookie_compliance_content_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Content settings'),
      '#open' => TRUE,
    ];

    $form['oum_cookie_compliance_content_settings']['accept_minimal'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable "Only accept necessary cookies" button'),
      '#default_value' => $config->get('accept_minimal'),
      '#description' => t('Enables an extra button for the twig template.<br />You can use <code>accept_minimal</code> in your twig files.'),
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Save the values.
    $config = $this->config('oum_cookie_compliance.settings');
    $config->set('load_module_twig', $form_state->getValue('load_module_twig'))
      ->set('load_module_css', $form_state->getValue('load_module_css'))
      ->set('accept_minimal', $form_state->getValue('accept_minimal'))
      ->save();

    parent::submitForm($form, $form_state);

    Cache::invalidateTags(['oum_cookie_compliance:attachments']);
  }

}
