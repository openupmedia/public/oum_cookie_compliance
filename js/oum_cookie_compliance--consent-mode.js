(function ($, Drupal, drupalSettings) {
  "use strict";
  // console.log('hooks loaded');
  
  // Sep up the namespace as a function to store list of arguments in a queue.
  Drupal.eu_cookie_compliance = Drupal.eu_cookie_compliance || function() {
    (Drupal.eu_cookie_compliance.queue = Drupal.eu_cookie_compliance.queue || []).push(arguments)
  };

  // Initialize the object with some data.
  Drupal.eu_cookie_compliance.a = +new Date;

  /**
   * Prepares data, and send it to datalayer.
   * @private
   */
  var _processCategories = function(response) {
    // get allowed categories from eu_cookie_compliance response
    var selectedcats = response.currentCategories;
    
    // prepare cookie consent datalayer event
    // if category allowed, set to 1, otherwise set it to 0
    var consent = {
      'event': 'cookie_consent',
      'analytics_consent': selectedcats.indexOf('analytics') !== -1 ? 'granted' : 'denied',
      'marketing_consent': selectedcats.indexOf('marketing') !== -1 ? 'granted' : 'denied',
      'functional_consent': selectedcats.indexOf('functional') !== -1 ? 'granted' : 'denied',
      'personalization_consent': selectedcats.indexOf('personalization') !== -1 ? 'granted' : 'denied',
      'security_consent': selectedcats.indexOf('security') !== -1 ? 'granted' : 'denied'
    };

    // create datalayer & push the custom Cookie Consent event to datalayer
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push(consent);

    // Update Google Consent Mode on datalayer
    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('consent', 'update', {
      'ad_storage': consent.marketing,
      'analytics_storage': consent.analytics,
      'functionality_storage': consent.functional,
      'personalization_storage': consent.personalization,
      'security_storage': consent.security
    });
  }

  /** 
   * Load saved cookie preferences
   */
  var postPreferencesLoadHandler = function(response) {
    // console.log('hook ran - postPreferencesLoadHandler');
    _processCategories(response);
  };
  Drupal.eu_cookie_compliance('postPreferencesLoad', _processCategories);

  /** 
   * Visitor saves cookie preferences
   */
  var postPreferencesSaveHandler = function(response) {
    // console.log('hooks ran - postPreferencesSaveHandler');
    _processCategories(response);
  };
  // eu_cookie_compliance post save hook is run
  Drupal.eu_cookie_compliance('postPreferencesSave', _processCategories);

  

})(jQuery, Drupal, drupalSettings);
