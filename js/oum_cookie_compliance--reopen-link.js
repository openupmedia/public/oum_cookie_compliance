/*global Drupal, jQuery*/
Drupal.behaviors.oumccReopenLink = {
  attach: function (context, settings) {
    (function ($) {

      /**
       * Show popup
       * 
       * If popup exists, open it. If it doesn't exist create a new popup.
       */
      function show_eucc_popup() {
        var popup = document.getElementById('sliding-popup');
        if (popup) {
          popup.dispatchEvent(new Event('eu_cookie_compliance_popup_open'));
          document.body.classList.add('eu-cookie-compliance-popup-open');
        }
        else {
          if (window.matchMedia('(max-width: ' + settings.eu_cookie_compliance.mobile_breakpoint + 'px)').matches
              && settings.eu_cookie_compliance.use_mobile_message) {
            Drupal.eu_cookie_compliance.createPopup(settings.eu_cookie_compliance.mobile_popup_html_info, false);
          } else {
            Drupal.eu_cookie_compliance.createPopup(settings.eu_cookie_compliance.popup_html_info, false);
          }
          Drupal.eu_cookie_compliance.initPopup();
        }
      }

      /**
       * Trigger cookie popup
       * 
       * Re-open the popup when a user clicks on a link that has a #eucc-open fragment as href.
       */
      var links = document.querySelectorAll('a[href="#eucc-open"]');
      for (var link of links) {
        link.addEventListener('click', (e) => {
          e.preventDefault();
          show_eucc_popup();
          document.getElementById('cookie-banner__toggler').checked = true;
        });
      }

    }(jQuery));
  }
};
