/*global Drupal, jQuery*/
Drupal.behaviors.oumccAcceptMinimal = {
  attach: function (context, settings) {
    (function ($) {

      // Init accept minimal link when the popup has opened
      var body = $('body');
      body.once('euccOumOpen').on('eu_cookie_compliance_popup_open', function() {
        $('.eu-cookie-compliance-banner-info').once('oumccAcceptMinimal').each(function () {

          var acceptMinimalLink = document.getElementById('oumcc-accept-minimal');
          var categoryChecks = document.querySelectorAll('.cookie-banner__cat-checkbox:not(.disabled) input[type="checkbox"]');
          acceptMinimalLink.addEventListener('click', (e) => {
            e.preventDefault();
            for (var categoryCheck of categoryChecks) {
              categoryCheck.checked = this.false;
            }
            document.querySelector('.eu-cookie-compliance-save-preferences-button').click();
          });

        });
      });

    }(jQuery));
  }
};
